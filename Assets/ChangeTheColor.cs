﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeTheColor : MonoBehaviour
{
    public Color Blue;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frameC:\Users\miharu\Documents\Unity\painting-ball\Assets\
    void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.transform.tag == "Player")
        {
            gameObject.GetComponent<Renderer>().material.color = Blue;
        }
    }
}