﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum Status : int
{
    BLUE = 0,
    SKY_BLUE = 1,
    VIVID = 2
}

public class PlayerController : MonoBehaviour
{
    Status status = Status.SKY_BLUE;
    private Rigidbody2D rb2d;
    private float t = 1f;
    private Vector3 puposePos;
    private Vector3 startPos;
    public float speed = 2f;
    private bool isMove = false;
    public Text clearText;
    public Text gameoverText;
    public Text countBlue;
    public Text countSkyBlue;
    public Text countVivid;
    public int blueCount;
    public int skyblueCount;
    public int vividCount;
    private bool postBlue = false;
    private bool postSkyblue = false;
    private bool postVivid = false;
    private bool preBlue = false;
    private bool preSkyblue = false;
    private bool preVivid = false;
    private bool passBlue = false;
    private bool passSkyblue = false;
    private bool passVivid = false;
    ChangeTheColor ctc;
    public Color Blue;
    public Color Skyblue;
    public Color Vivid;


    // Use this for initialization
    private void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();

        clearText.text = "";
        gameoverText.text = "";

        SetBlueCountText();
        SetSkyBlueCountText();
        SetVividCountText();
    }

    // Update is called once per frame
    private void Update()
    {
        if (isMove)
        {
            Move();
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                MoveInit(transform.position + new Vector3(0, 2, 0));
            }
            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                MoveInit(transform.position + new Vector3(0, -2, 0));

            }
            if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                MoveInit(transform.position + new Vector3(2, 0, 0));
            }
            if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                MoveInit(transform.position + new Vector3(-2, 0, 0));
            }
        }

        TotalCount();
        SetBlueCountText();
        SetSkyBlueCountText();
        SetVividCountText();

        if (blueCount <= -1 || skyblueCount <= -1 || vividCount <= -1)
        {
            gameoverText.text = "GAMEOVER";
            gameObject.SetActive(false);
        }

        GameObject[] bluegrounds = GameObject.FindGameObjectsWithTag("Blue");
        GameObject[] skybluegrounds = GameObject.FindGameObjectsWithTag("SkyBlue");
        GameObject[] vividgrounds = GameObject.FindGameObjectsWithTag("Vivid");

        transform.position = new Vector3(Mathf.Clamp(transform.position.x, -3, 3), Mathf.Clamp(transform.position.y, -3, 3), transform.position.z);
    }

    private void MoveInit(Vector3 purposePos)
    {
        isMove = true;
        this.puposePos = purposePos;
        startPos = transform.position;
        t = 0f;
    }

    private void Move()
    {
        t = Mathf.Clamp(t + Time.deltaTime * speed, 0, 1);
        transform.position = Vector3.Lerp(startPos, puposePos, t);
        if (1f - t < Mathf.Epsilon)
        {
            isMove = false;
            transform.position = puposePos;
            preBlue = false;
            preSkyblue = false;
            preVivid = false;
            postBlue = false;
            postSkyblue = false;
            postVivid = false;
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(blueCount==0&&skyblueCount==0&&vividCount==0)
        {
            if (other.gameObject.CompareTag("ClearGround"))
            {
                clearText.text = "CLEAR";
            }
        }
        

        if (other.gameObject.CompareTag("Blue"))
        {
            postBlue = true;
        }

        if (other.gameObject.CompareTag("SkyBlue"))
        {
            postSkyblue = true;
        }

        if (other.gameObject.CompareTag("Vivid"))
        {
            postVivid = true;
        }

        if (other.gameObject.CompareTag("Gameover"))
        {
            gameoverText.text = "GAMEOVER";
            gameObject.SetActive(false);
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("SkyBlue") || other.gameObject.CompareTag("Vivid"))
        {
            preBlue = true;
            other.gameObject.tag = ("Gameover");
        }

        if (other.gameObject.CompareTag("Blue") || other.gameObject.CompareTag("Vivid"))
        {
            preSkyblue = true;
            other.gameObject.tag = ("Gameover");
        }

        if (other.gameObject.CompareTag("Blue") || other.gameObject.CompareTag("SkyBlue"))
        {
            preVivid = true;
            other.gameObject.tag=("Gameover");
        }
    }


    void SetBlueCountText()
    {
        countBlue.text = "Blue" + blueCount.ToString();
    }
    void SetSkyBlueCountText()
    {
        countSkyBlue.text = "SkyBlue" + skyblueCount.ToString();
    }
    void SetVividCountText()
    {
        countVivid.text = "Vivid" + vividCount.ToString();
    }

    void TotalCount()
    {
        if (preBlue&&postBlue)
        {
            blueCount = blueCount - 1;
            SetBlueCountText();
            preBlue = false;
            postBlue = false;
        }

        if (preSkyblue&&postSkyblue)
        {
            skyblueCount = skyblueCount - 1;
            SetSkyBlueCountText();
            preSkyblue = false;
            postSkyblue = false;
        }

        if (preVivid&&postVivid)
        {
            vividCount = vividCount - 1;
            SetVividCountText();
            preVivid = false;
            postVivid = false;
        }
    }
}
